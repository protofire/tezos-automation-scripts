<p align="center">
  <img width="40%" height="40%" src="logo.png">
</p>

# [Protofire.io](https://protofire.io/) Tezos automation scripts

[![pipeline status](https://gitlab.com/protofire/tezos-automation-scripts/badges/master/pipeline.svg)](https://gitlab.com/protofire/tezos-automation-scripts/commits/master)

A set of scripts to simplify the process of Tezos node(s) deployment.

# Description

This repository contains three different automation solutions. 

## Kubernetes cluster for Tezos

This solution allow users to spin-up “out-of-the-box” cluster in a few commands. 

Using our bash script Tezos community members will be able to use managed Kubernetes (either at AWS, GCP or Azure) to run an up-to-date Tezos nodes based on the docker images. It simplifies the virtual infrastructure
setup and the cluster management also. The designed Kubernetes autoupdater gives a possibility to update running containers automatically. 

Check [the detailed README](kubernetes/README.md) for usage instructions.

## Continuous Integration to recreate images for cloud VMs

We believe it is useful for the Tezos ecosystem to have more options for deploying
Tezos, besides existing docker images. Same as with docker, the [Cloud images](https://gitlab.com/tezos/tezos/merge_requests/808) will be continuously built from the most recent tested codebase. 


Same as Tezos, we are actively using the GitLab CI to automate images builds (check [config file](.gitlab-ci.yml) and [README](packer/README.md) for details). As an assembling tool Packer by HashiCorp is used. Images are recreated each time new commits appears at the production branches at Tezos main repository.


As a result, single node users will have a choice between docker images, cloud images and manual full cycle installation.


## 1-click deployment of Tezos node in AWS with Terraform

Also, the community users who uses the AWS cloud provider and have some skills in Terraform tool can try to deploy Tezos infrastructure using our solution. Once the deployment process will be finished an up-to-date Tezos node will be created along with the security group. The dedicated VPC and subnet can be created optionally as well.


For installation and usage see the following [README](terraform/README.md).

# About us

We are Protofire, a blockchain unit of Altoros Americas LLC.


This repository was created to support and automate this [project](https://gitlab.com/tezos/tezos) and is a result of the grant ‘Automate Tezos Setup and Management Process for All Popular Clouds’ issued
by the Tezos Foundation on 10/25/2018. 


In our work we used different popular DevOps technologies such as K8S, Terraform, Packer, GitLab CI, Docker.

# Project status

The development on current SoW is finished. Accepting issues and PRs.

# Contributing

Feel free to contribute by opening issues and PR at this repository.

# License

This repository has the same [LICENSE](https://gitlab.com/tezos/tezos/blob/master/LICENSE) as the main Tezos repository.