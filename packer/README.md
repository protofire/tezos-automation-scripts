# Packer scripts

Prerequisites for Packer builder:

* Azure CLI
* JQ
* Packer

Packer config file contain three builders (one per cloud provider), three post-processors and three provisioners:

* Post-processors:
  * `googlecompute-export` - uploads created Tezos image at GCP to the public bucket;
  *  `manifest` - creates an artifact with the results of the all builds;
  * `shell-local` - copy created Tezos image disk at Azure to the public bucket. Requires Azure CLI
* Provisioners:
  *  `bootstrap.sh` creates Tezos node at VM. Includes software installation, binaries generation, service installation.
  * `test.sh`  checks if Tezos node is starting;
  * The purpose of the last provisioner is to generalize Azure VM in accordance to best practice guides.

## Variables
Packer requires the following environment variables to be set:

* `aws_access_key`, `aws_secret_key`:  security credentials from AWS cloud provider;
* `azure_client_id`, `azure_subscription_id`, `azure_tenant_id`, `azure_client_secret`: security credentials from Azure cloud provider;
* `azure_storage_account` - a name of storage account where built image will be stored;
* `azure_resource_group_name`: existing resource group where `azure_storage_account` do exists;
* `azure_container`: azure container name;
* `azure_disk_size`: unlike other cloud providers Azure disk size cannot be simply changed during image deploying process. To simplify process of node installation it is highly recommended to assemble an image with sufficient space for each network;
* `gcp_project`: a GCP project where image will be assembled;
* `gcp_storage`: google bucket name;
* `opam_version`: version of opam compiler to be used during assembling process;
* `tezos_network`: `mainnet`, `alphanet`,`zeronet` or any other network accordingly.

Also requires `account.json` file with a credentials to Google Service account with at least Compute Engine Admin, Service Account User and Storage Admin rights.

# CI

If you want to setup GitlabCI on Packer images you will have to configure the network variables above as a project variables. Also, in order to build images on GCP one may want to specify `account.json` file. To do this execute the following command:

```bash
cat account.json | base64 -w0
```

The output of this command should be set as a `google_account_file_content` project variable. 

One may want also to download and install an `checker.sh` script from `packer` folder. This script will check for new commits on target network branches and trigger pipelines when necessary. To activate script simply add it to the crontab. Usually you would like to setup it as follows:

## Crontab

Usually you may wan to install the following crontab to check for updates on each network:
```bash
0 0 * * * /home/ubuntu/checker.sh zeronet <project_id>
0 1 * * * /home/ubuntu/checker.sh alphanet <project_id>
0 2 * * * /home/ubuntu/checker.sh mainnet <project_id>
```
where `<project_id>` is an unique ID of your project on GitLab.

## Manual usage

You can use the config file in this folder to build images manually. First of all you will have to install all necessary prerequisites:
```bash
sudo apt-get update && sudo apt-get install git jq unzip apt-transport-https lsb-release software-properties-common dirmngr -y
cd /tmp
wget https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_linux_amd64.zip
unzip packer_1.3.3_linux_amd64.zip
sudo mv packer /usr/local/bin/
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | sudo tee /etc/apt/sources.list.d/azure-cli.list
sudo apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv --keyserver packages.microsoft.com --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF
sudo apt-get update && sudo apt-get install azure-cli -y
```
Clone the repo and go to the packer folder:
```bash
cd /home/ubuntu #or any other user instead
git clone https://gitlab.com/protofire/tezos-automation-scripts
cd tezos-automation-scripts/packer
```
Create variables.json file with the following content:
```json
{
  "<variable1>":"<value1>",
  ...
  "<variableN>":"<valueN>"
}
```
Replace `<variable>` entries with all the necessary variables from the [variables](#variables) section. Also you can add and modify the following variables which are related to build and deployment process:
```json
{
        "aws_build_region": "us-east-2",
        "gcp_build_zone": "us-central1-a",
        "azure_build_region": "eastus",
        "destination_regions": "us-east-2,us-east-1,us-west-1,us-west-2,ap-south-1,ap-northeast-2,ap-southeast-1,ap-southeast-2,ap-northeast-1,ca-central-1,eu-central-1,eu-west-1,eu-west-2,eu-west-3,eu-north-1,sa-east-1,us-east-1"
}
```
Launch build:
```bash
packer build -var-file=variables.json packer.json
```