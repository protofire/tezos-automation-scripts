#!/bin/bash
  
echo "Checking commits at $1 branch"

azure_disk_size=50
case $1 in
  mainnet)
          azure_disk_size=100
esac


curl "https://gitlab.com/api/v4/projects/3836952/repository/commits?ref_name=$1" > "$1_latest.git"

 if [ ! -s $1_old.git ]; then
    cp $1_latest.git $1_old.git;
  fi;

  if cmp -s $1_latest.git $1_old.git; then
    echo "No new commits were found";
  else
    cp $1_latest.git $1_old.git;
    echo "New commits were found! Rebuildingg images!";
    curl -X POST -F token=$token -F variables[tezos_network]=$1 -F variables[azure_disk_size]=$azure_disk_size -F ref=master https://gitlab.com/api/v4/projects/$2/trigger/pipeline
  fi;

echo "Check finished.";
