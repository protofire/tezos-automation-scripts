#!/bin/bash
set -e
echo "Generating Tezos identity"
/home/ubuntu/tezos-identity-generator 
cp /home/ubuntu/tezos-identity-generator.backup /home/ubuntu/tezos-identity-generator
chmod a+x /home/ubuntu/tezos-identity-generator
echo "Identity generated. Starting Tezos node"
sudo service tezos start
sudo service accuser@tezos start
sudo service baker@tezos start
sudo service endorser@tezos start
echo "Node started. Testing if node is syncing"
timeout 10m tail -n 10000000000 -f /var/log/syslog | grep -m 1 "The Tezos node is now running"
echo "Tested. Stopping services and removing Tezos data"
sudo service accuser@tezos stop
sudo service baker@tezos stop
sudo service endorser@tezos stop
sudo service tezos stop
echo "Services has been stopped."
sudo rm /home/ubuntu/.tezos-node 2> /dev/null || true
sudo rm /home/ubuntu/.tezos-client 2> /dev/null || true
echo "Data has been removed"
