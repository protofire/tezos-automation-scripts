#!/bin/bash
set -e
echo "Initial script started."

sudo apt-get update && sudo apt-get update && sudo apt-get install -yy python-minimal rsync git m4 build-essential patch unzip bubblewrap wget libgmp-dev libhidapi-dev pkg-config libev-dev && echo "Apps set installed." && \

echo "Installing opam" && \
sudo wget -O /usr/local/bin/opam https://github.com/ocaml/opam/releases/download/$OPAM_VERSION/opam-$OPAM_VERSION-x86_64-linux && \
sudo chmod a+x /usr/local/bin/opam && \
echo "Opam installed" && \

echo "Clonning Tezos repo" && \
git clone https://gitlab.com/tezos/tezos.git /home/ubuntu/tezos && \
cd /home/ubuntu/tezos && \
git checkout $NETWORK && \
echo "Repo cloned" && \

echo "Initializing opam" && \
opam init --bare --yes && \
echo "Opam initialized" && \
echo "Building Tezos dependencies..." && \
sed -i -e 's/--deps-only --with-test/--deps-only --yes --with-test/g' /home/ubuntu/tezos/scripts/install_build_deps.raw.sh && \
make build-deps && \
echo "Dependencies installed" && \
eval $(opam env) && \
make && \
echo "Tezos has been built" && \

echo "Installing Tezos as a service"
cat <<EOT >> /home/ubuntu/.bashrc && \
export PATH=~/tezos:$PATH
source /home/ubuntu/tezos/src/bin_client/bash-completion.sh
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y
EOT

source ~/.bashrc

sudo sh -c "cat >> /etc/systemd/system/tezos.service" <<-EOT 
[Unit]
Description=Tezos $NETWORK node service
After=network.target

[Service]
Type=simple
User=ubuntu
WorkingDirectory=/home/ubuntu/tezos
ExecStartPre=-/home/ubuntu/tezos-identity-generator
ExecStart=/home/ubuntu/tezos/tezos-node run
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOT

sudo sh -c "cat >> /etc/systemd/system/accuser@tezos.service" <<-EOT
[Unit]
Description=Tezos $NETWORK node accuser
After=tezos.service

[Service]
Type=simple
User=ubuntu
WorkingDirectory=/home/ubuntu/tezos
ExecStartPre=/bin/sleep 60
ExecStart=/bin/bash -c "ls /home/ubuntu/tezos/tezos-accuser* | xargs -L1 -P0 -I cmd bash -c cmd\\ run"
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOT

sudo sh -c "cat >> /etc/systemd/system/baker@tezos.service" <<-EOT
[Unit]
Description=Tezos $NETWORK baker service
After=tezos.service

[Service]
Type=simple
User=ubuntu
WorkingDirectory=/home/ubuntu/tezos
ExecStartPre=/bin/sleep 60
ExecStart=/bin/bash -c "ls /home/ubuntu/tezos/tezos-baker* | xargs -L1 -P0 -I cmd bash -c cmd\\ run\\ with\\ local\\ node\\ /home/ubuntu/.tezos-node"
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOT

sudo sh -c "cat >> /etc/systemd/system/endorser@tezos.service" <<-EOT
[Unit]
Description=Tezos $NETWORK endorser service
After=tezos.service

[Service]
Type=simple
User=ubuntu
WorkingDirectory=/home/ubuntu/tezos
ExecStartPre=/bin/sleep 60
ExecStart=/bin/bash -c "ls /home/ubuntu/tezos/tezos-endorser* | xargs -L1 -P0 -I cmd bash -c cmd\\ run"
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOT

sudo systemctl daemon-reload && \
sudo systemctl enable tezos.service && \
sudo systemctl enable accuser@tezos.service && \
sudo systemctl enable endorser@tezos.service && \
sudo systemctl enable baker@tezos.service

cat <<EOT >> /home/ubuntu/tezos-identity-generator
#!/bin/bash

/home/ubuntu/tezos/tezos-node identity generate
/home/ubuntu/tezos/tezos-node config init
/home/ubuntu/tezos/tezos-node config update --rpc-addr=0.0.0.0:8732

# Delete me
rm /home/ubuntu/tezos-identity-generator
EOT

cp /home/ubuntu/tezos-identity-generator /home/ubuntu/tezos-identity-generator.backup && \
sudo chmod a+x /home/ubuntu/tezos-identity-generator

sudo sh -c "cat >> /etc/cron.daily/update.sh" <<-EOT
#!/bin/bash

#Variable declaration
tezos_dir="/home/ubuntu/tezos/"
branch=$NETWORK
opam_version=\$(cat /home/ubuntu/tezos/scripts/version.sh | grep opam_version | cut -d '=' -f2)
opam_current_version=\$(opam --version | cut -d '.' -f1,2)
opam_install_version=\$(echo \$opam_version | sed 's/$/.0/')

check_update () {

       cd \${tezos_dir}

       git fetch
       if [[ \$(git checkout \$branch 2>1 | grep 'git pull' | wc -l) -gt 0  ]]; then
               git pull
       else
           logger -s 'No updates found'
           exit 0;
       fi

}

update () {

   sudo systemctl stop *tezos.service
   cd \${tezos_dir}
   make clean
   rm -f tezos-*
   opam init --bare --yes
   yes Y | make build-deps
   eval \$(opam env)
   make
   sudo systemctl start tezos.service
   sudo systemctl start endorser@tezos.service
   sudo systemctl start baker@tezos.service
   sudo systemctl start accuser@tezos.service

}

update_opam () {

   sudo rm /usr/local/bin/opam
   sudo wget -O /usr/local/bin/opam https://github.com/ocaml/opam/releases/download/\${opam_install_version}/opam-\${opam_install_version}-x86_64-linux
   sudo chmod a+x /usr/local/bin/opam

}

if [[ \$TEZOS_MAINNET_DO_NOT_PULL == 'yes' ]]; then

      logger -s 'User has disabled automatic updates. Unset \$TEZOS_MAINNET_DO_NOT_PULL variable to enable updates.'
      exit 1;
fi

check_update

if [ "\${opam_version}" != "\${opam_current_version}" ]; then
update_opam
fi

update

exit 0
EOT

sudo chmod a+x /etc/cron.daily/update.sh && \
sudo touch /var/log/tezos-update.log && \
sudo chmod -R ugo+rw /var/log/tezos-update.log && \
echo "Tezos installed"

echo "Node launched. Script finished. Please, check the log above for any errors that might occur during the installation."
