#!/bin/bash
set -e

usage() {
    echo "Usage: $0 <check_period_in_seconds> [<owner>/<repo> <tag> <deployment_type>=<name>]"
}

if [ "$#" -lt 4 ] ; then usage ; exit 1; fi

sleep_time=$1
shift

while true; do

  sleep $sleep_time; 
  echo "Starting images check..."; 
  while [ $# -gt 0 ]; do
  do
    curl --silent --header "Accept: application/vnd.docker.distribution.manifest.v2+json" --header "Authorization: Bearer $(curl --silent "https://auth.docker.io/token?scope=repository:$1:pull&service=registry.docker.io" | jq -r ".token")" "https://registry-1.docker.io/v2/$1/manifests/$2" > /var/run/latest_update; 

    if [ ! -s /var/run/old_update ]; then 
      cp /var/run/latest_update /var/run/old_update; 
    fi; 

    if cmp -s /var/run/latest_update /var/run/old_update; then 
      echo "No new images were found";
    else
      cp /var/run/latest_update /var/run/old_update;
      echo "New images were found! Recreating pods!"; 
        kubectl get $(echo $3|cut -d'=' -f 1) $(echo $3|cut -d'=' -f 2) -o yaml --token=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) | kubectl replace --token=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) --force -f -; 
    fi;
  done;  

echo "Check finished."; 

done
