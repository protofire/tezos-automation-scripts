# Dockerfiles

This folder contains a source Dockerfile for [image at DockerHub](https://hub.docker.com/r/altorosdev/k8s-autoupdater). The image is designed to be an autoupdater for any K8S containers. It will automatically update any specified container at chosen Kubernetes cluster every time container's image updates on DockerHub.

## Prerequisites

In order to make autoupdater function properly one should preconfigure the following:

1. Create a Kubernetes service user account with rights to create, remove and replace Deployments/StatefulSets/etc. 
2. Add to all the services that you want to be managed by autoupdater the `imagePullPolicy: always` policy.
3. Attach a small (few MB will be usually enough) persistent volume to the `/var/run` folder at autoupdater container.

## Usage

Docker image has an script entrypoint that accepts variables in the following order:
`<refresh_time> [<organization>/<repository> <tag> <deployment_type>=<name> ...]`, where
* `refresh_time` - is a period to check for selected container(s) images update;
* `organization` stands for organization name on DockerHub. Can be skipped along with `/` for Docker official images;
* `repository` is a repository name;
* `tag` is a tag to check (`latest`, for example);
*  `<deploymeht_type>` is a Kubernetes deployment type such as `StatefulSet`, `Deployment`, etc.
*  `<name>` is a name of the Deployment.

Once image is deployed to the Kubernetes cluster it will start periodical check for image updates that will trigger the image repull once it updates on the Dockerhub.

## YAML
Usually, you want to do the following:
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: autoupdater-secret
  annotations:
    kubernetes.io/service-account.name: autoupdater
type: kubernetes.io/service-account-token

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: autoupdater
  namespace: default

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: autoupdater-roles
  namespace: default
rules:
- apiGroups:
  - apps
  - extensions
  resources:
  - statefulsets
  - deployments
  - daemonsets
  verbs:
  - replace
  - get
  - delete
  - create

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: autoupdater-binding
  namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: autoupdater-roles
subjects:
- kind: User
  name: system:serviceaccount:default:autoupdater
  namespace: default
  apiGroup: rbac.authorization.k8s.io

---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: tezos-autoupdater
  namespace: default
  labels:
    app: my-app
    component: autoupdater
spec:
  selector:
    matchLabels:
      app: my-app
      component: autoupdater
  template:
    metadata:
      labels:
        app: my-app
        component: autoupdater
    spec:
      serviceAccountName: autoupdater
      containers:
      - name: autoupdater
        image: altorosdev/k8s-autoupdater
        command:
        - sh
        - -c
        - "/usr/local/bin/entrypoint.sh 86400 myorg/myrepo latest statefulset=node altorosdev/k8s-autoupdater latest daemonset=autoupdater"
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/"
      volumes:
      - name: node-data
        persistentVolumeClaim:
          claimName: shared-storage
      - name: kubersecretconfig
        secret:
          secretName: kubersecret
          items:
          - key: config
            path: config

```