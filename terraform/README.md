# Terraform scripts for AWS

Terraform scripts designed to deploy a Tezos node along with set of securitry rules and dedicated VPC (if necessary).

## Files description

* `bootstrap.sh` - init script that are loading into instantiating VM.

* `eip.tf` - contains definition of elastic ip address that is assigned to instantiated VM

* `network.tf` - contains definition of dedicated VPC and a data file for existing subnet 

* `provider.tf` - contains AWS provider-related settings including region and credentials.

* `security_groups.tf` - contains set of firewall rules that applies to instantiated VM

* `instance.tf` - contains description of instance including AMI description and combining all the resources above.

* `variables.tf` - contains a following set of variables:
  * `region`/`availability_zone`  - location where instance should be launched;
  * `subnet_id` - id of the existing subnet. If set to `new` a new `VPC` and a `subnet` will be created;
  * `aws_access_key`/`aws_secret_key` - authentication credentials for AWS;
  * `instance_type` - type of instance to launch;
  * `key_name` - SSH key name uploaded to selected AWS region;
  * `rpc_port` - rpc port exposed at created instance;
  * `p2p_port` - p2p port exposed at created instance;
  * `ip_whitelist` - a list of IP addresses to allow RPC and SSH calls to;
  * `network` - a type of network to connect instance to (mainnet,zeronet,alphanet).

# Usage

First of all install Terraform:
```bash
cd /tmp
wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip
sudo apt-get update && sudo apt-get install unzip -y
unzip terraform_0.11.11_linux_amd64.zip
sudo mv terraform /usr/local/bin/
```
Then install Git and clone this repo:
```bash
cd /home/ubuntu #or any other user instead
sudo apt-get install git -y
git clone https://gitlab.com/protofire/tezos-automation-scripts
```
Go to the `terraform` folder and apply the config
```bash
cd tezos-automation-scripts/terraform
terraform apply
```
Specify all the necessary variables through the interactive prompt to start deployment process.

