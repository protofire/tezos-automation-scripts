#!/bin/bash
echo "Initial script started.";
export LC_ALL=C;
echo "Installing docker.";
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -;
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $$(lsb_release -cs) stable";
apt-get update -y;
apt-get install -y python-minimal docker-ce;
echo "Docker installed.";
usermod -aG docker ubuntu;
systemctl enable docker;
systemctl start docker;
echo "Docker installed as a service. Installing docker-compose...";
curl -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose;
chmod +x /usr/local/bin/docker-compose;
docker-compose --version;
echo "Docker-compose installed. Pulling and executing Tezos script.";
wget -O /usr/local/bin/${network}.sh https://gitlab.com/tezos/tezos/raw/master/scripts/alphanet.sh;
chmod +x /usr/local/bin/${network}.sh;
/usr/local/bin/${network}.sh --port ${p2p_port} start --rpc-port ${rpc_port};
echo "Node launched. Script finished. Please, check the log above for any errors that might occur during the installation."
