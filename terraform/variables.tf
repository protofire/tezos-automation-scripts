variable "region" {
  type = "string"
  default = "us-east-1"
  description = "Location, where EC2 instance with Tezos on board will be launched"
}

variable "availability_zone" {
  type = "string"
  default = "us-east-1e"
  description = "Zone, where EC2 instance with Tezos on board will be launched"
}

variable "subnet_id" {
  type = "string"
  default = "new"
  description = "Subnet, where EC2 instance with Tezos on board will be launched. If is set to `new` then new vpc and subnet will be created"
}

variable "aws_access_key" {
  type = "string"
  description = "Access key of AWS user with sufficient rights (see readme for details)"
}

variable "aws_secret_key" {
  type = "string"
  description = "Private part of the access key"
}

variable "instance_type" {
  type = "string"
  default = "m3.large"
  description = "Size and type of the instance to launch. Defaults to general purpose instance with 2vCPUs and 8 GB of RAM"
}

variable "key_name" {
  type = "string"
  description = "Name of the SSH key uploaded to AWS that will be used as an access key for created VM" 
}

variable "rpc_port" {
  type = "string"
  default = "8732"
}

variable "p2p_port" {
  type = "string"
  default = "9732"
}

variable "ip_whitelist" {
  type = "list"
  default = ["0.0.0.0/0"]
  description = "A list of CIDR blocks that will be able to communicate with node via RPC port"
}

variable "network" {
  type = "string"
  default = "alphanet"
  description = "A network to launch Tezos node at. May be either mainnet, alphanet or zeronet" 
}
