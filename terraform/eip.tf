resource "aws_eip" "ip" {
  instance = "${aws_instance.node-tezos.id}"
  vpc      = true

  tags = {
    Name = "Tezos"
  }
}

