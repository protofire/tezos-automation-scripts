resource "aws_security_group" "security-tezos" {
  name = "Tezos"
  tags {
        Name = "Tezos"
  }

  vpc_id = "${element(concat(aws_vpc.new.*.id,data.aws_subnet.existing.*.vpc_id), 0)}"

  ingress {
    from_port   = "${var.rpc_port}"
    to_port     = "${var.rpc_port}"
    protocol    = "tcp"
    cidr_blocks = "${var.ip_whitelist}"
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = "${var.ip_whitelist}"
  }

  ingress {
    from_port   = "${var.p2p_port}"
    to_port     = "${var.p2p_port}"
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
