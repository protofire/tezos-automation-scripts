data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

data "template_file" "bootstrap" {
  template = "${file("bootstrap.sh")}"
  vars {
    network  = "${var.network}"
    p2p_port = "${var.p2p_port}"
    rpc_port = "${var.rpc_port}"
  }
}

resource "aws_instance" "node-tezos" {
  tags {
        Name = "Tezos"
  }
  ami = "${data.aws_ami.ami.id}"
  availability_zone = "${var.availability_zone}"
  subnet_id="${var.subnet_id == "new"? element(concat(aws_subnet.new.*.id,list("")), 0) : var.subnet_id }"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  user_data = "${data.template_file.bootstrap.rendered}"
  security_groups = ["${aws_security_group.security-tezos.id}"]
}
