resource "aws_vpc" "new" {
  count            = "${var.subnet_id == "new"? 1 : 0 }"
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "Tezos"
  }
}



resource "aws_route_table" "r" {
  vpc_id = "${aws_vpc.new.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.new.id}"
  }

  tags = {
    Name = "Tezos"
  }
}

resource "aws_subnet" "new" {
  count             = "${var.subnet_id == "new"? 1 : 0 }"
  vpc_id            = "${aws_vpc.new.id}"
  cidr_block        = "10.0.0.0/24"
  availability_zone = "${var.availability_zone}"

  tags = {
    Name = "Tezos"
  }
}

data "aws_subnet" "existing" {
  count = "${var.subnet_id == "new"? 0 : 1}"
  id    = "${var.subnet_id}"

  tags = {
    Name = "Tezos"
  }
}

resource "aws_internet_gateway" "new" {
  count  = "${var.subnet_id == "new"? 1 : 0 }"
  vpc_id = "${aws_vpc.new.id}"

  tags = {
    Name = "Tezos"
  }
}
