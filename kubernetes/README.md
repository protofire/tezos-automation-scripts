# Bash K8S script

Tezos.bash script provides end users with a simple way to create a Kubernetes cluster at any of most popular cloud providers (AWS/GCP/Azure) and/or install a set of Tezos nodes there. 
Scripts always install an accuser, baker, endorser and at least on private and one public node. 
Amount of public nodes can be increased via `-N` flag. 
One can also customize build via others options described at the next section.

## Usage

```bash
./tezos.bash [OPTIONS]
Required arguments are limited to:
  -c, --cluster <string>
      The name of the cluster to use
  -p, --provider <GCP|AWS|Azure>
      Provider to setup cluster at
  -r, --region <string>
      Region to setup K8S cluster at. Should be specified in valid for chosen provider format. 
      Example: us-east1 (for GCP); us-east-1 (for AWS); eastus (for Azure).
Optional arguments are:
  -N, --nodes <int>
      Ammount of Tezos nodes to create. Defaults to 1
  -a, --action <apply|patch|destroy|rebuild>
      Specifies the desired action. Defaults to apply;
  -n, --network <string>
      Name of the network (zeronet, alphanet, etc.). Defaults to zeronet
  --rpc <int>
      Specifies the rpc port for node management. Defaults to 8732
  -s, --namespace <string>
      Specifies namespace to use. `Default` by default :)
  -V, --volume <int>
      Explicitly specifies volume size in Gbytes for node data. Defaults to 100Gb
  --dry-run
      Do not apply config. Prepare it and place in the folder where script is located.
```
     
## References

Thanks for [Daniel Smith](https://gitlab.com/lavalamp) for parts of his code we were using while developing our code.