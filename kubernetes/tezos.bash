#!/bin/bash

usage() {
    echo "Usage: $0 [OPTIONS]"
    echo "Required arguments are limited to:"
    echo "  -c, --cluster <string>"
    echo "      The name of the cluster to use"
    echo "  -p, --provider <GCP|AWS|Azure>"
    echo "      Provider to setup cluster at"
    echo "  -r, --region <string>"
    echo "      Region to setup K8S cluster at. Should be specified in valid for chosen provider format. "
    echo "      Example: us-east1 (for GCP); us-east-1 (for AWS); eastus (for Azure)."  
    echo "Optional arguments are:"
    echo "  -N, --nodes <int>" 
    echo "      Ammount of Tezos nodes to create. Defaults to 1" 
    echo "  -a, --action <apply|patch|destroy|rebuild>"
    echo "      Specifies the desired action. Defaults to apply;"
    echo "  -n, --network <string>"
    echo "      Name of the network (zeronet, alphanet, etc.). Defaults to zeronet'"
    echo "  --rpc <int>"
    echo "      Specifies the rpc port for node management. Defaults to 8732'"
    echo "  -s, --namespace <string>"
    echo "      Specifies namespace to use. Default by default :)"
    echo "  -V, --volume <int>"
    echo "      Explicitly specifies volume size in Gbytes for node data. Defaults to 100Gb"
    echo "  --dry-run"
    echo "      Do not apply config. Prepare it and place in the folder where script is located."
}

GoogleAuth()
{
	command -v gcloud >/dev/null 2>&1 || { echo >&2 "This script requires GCP CLI but it's not installed.  Aborting. Please, install GCP CLI as described here => https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu and then rerun script."; exit 5; }
	command -v kubectl >/dev/null 2>&1 || { echo >&2 "This script requires 'kubectl' but it's not installed.  Aborting. Please, install it using 'sudo apt install kubectl' command and then rerun script."; exit 5; }
	
	if [ $(gcloud auth list --filter=status:ACTIVE --format="value(account)" | wc -l) -lt 1 ]; then
		gcloud init --console-only
	fi	
}

AWSAuth()
{
	command -v aws >/dev/null 2>&1 || { echo >&2 "This script requires AWS CLI but it's not installed.  Aborting. Please, install AWS CLI as described here => https://docs.aws.amazon.com/cli/latest/userguide/installing.html and rerun script."; exit 5; }
	command -v aws-iam-authenticator >/dev/null 2>&1 || { echo >&2 "This script requires AWS IAM Authenthicator but it's not installed.  Aborting. Please, install AWS IAM Authenthicator from here => https://github.com/kubernetes-sigs/aws-iam-authenticator/releases and rerun script."; exit 5; }
	command -v kubectl >/dev/null 2>&1 || { echo >&2 "This script requires 'kubectl' but it's not installed.  Aborting. Please, install it using 'sudo apt install kubectl' command and rerun script."; exit 5; }
	command -v eksctl >/dev/null 2>&1 || { echo >&2 "This script requires 'eksctl' but it's not installed.  Aborting. Please, install it using 'curl --silent --location \"https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz\" | tar xz -C /tmp && sudo mv /tmp/eksctl /usr/local/bin/' and rerun script."; exit 5; }

	aws sts get-caller-identity
	if [ $? -ne 0 ]; then	
		aws configure
	fi
}

AzureAuth()
{
	command -v az >/dev/null 2>&1 || { echo >&2 "This script requires Azure CLI but it's not installed.  Aborting. Please, install Azure CLI as described here => https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest and rerun script."; exit 5; }
	
	az account get-access-token
	if [ $? -ne 0 ]; then
                az login
        fi
}

AzureDeployScaler()
{
	ID=`az account show --query id -o json`
        SUBSCRIPTION_ID=`echo $ID | tr -d '"' `
        TENANT=`az account show --query tenantId -o json`
        TENANT_ID=`echo $TENANT | tr -d '"' | base64`
        CLUSTER_NAME=`echo $cluster | base64`
        RESOURCE_GROUP=`echo ${cluster}_rg | base64`
        PERMISSIONS=`az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/$SUBSCRIPTION_ID" -n $cluster -o json`
        CLIENT_ID=`echo $PERMISSIONS | sed -e 's/^.*"appId"[ ]*:[ ]*"//' -e 's/".*//' | base64`
        CLIENT_SECRET=`echo $PERMISSIONS | sed -e 's/^.*"password"[ ]*:[ ]*"//' -e 's/".*//' | base64`
        SUBSCRIPTION_ID=`echo $ID | tr -d '"' | base64 `
        NODE_RESOURCE_GROUP=`az aks show --name $cluster --resource-group ${cluster}_rg -o tsv --query 'nodeResourceGroup' | base64`
        NODE_POOL=`kubectl get nodes --show-labels | awk '{print $6}' | sed -n '2{p;q}' | perl -pe 's|(.*)(agentpool=)(.*?),(.*)|\3|'`

$(cat >| ./aks-cluster-autoscaler.yml <<-EOF
apiVersion: v1
kind: Secret
metadata:
    name: cluster-autoscaler-azure
    namespace: kube-system
data:
    ClientID: $CLIENT_ID
    ClientSecret: $CLIENT_SECRET
    ResourceGroup: $RESOURCE_GROUP
    SubscriptionID: $SUBSCRIPTION_ID
    TenantID: $TENANT_ID
    VMType: QUtTCg==
    ClusterName: $CLUSTER_NAME
    NodeResourceGroup: $NODE_RESOURCE_GROUP

---
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-addon: cluster-autoscaler.addons.k8s.io
    k8s-app: cluster-autoscaler
  name: cluster-autoscaler
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: kube-dashboard
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: rook-operator
  namespace: rook-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kube-dashboard
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-autoscaler
  labels:
    k8s-addon: cluster-autoscaler.addons.k8s.io
    k8s-app: cluster-autoscaler
rules:
- apiGroups: [""]
  resources: ["events","endpoints"]
  verbs: ["create", "patch"]
- apiGroups: [""]
  resources: ["pods/eviction"]
  verbs: ["create"]
- apiGroups: [""]
  resources: ["pods/status"]
  verbs: ["update"]
- apiGroups: [""]
  resources: ["endpoints"]
  resourceNames: ["cluster-autoscaler"]
  verbs: ["get","update"]
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["watch","list","get","update"]
- apiGroups: [""]
  resources: ["pods","services","replicationcontrollers","persistentvolumeclaims","persistentvolumes"]
  verbs: ["watch","list","get"]
- apiGroups: ["extensions"]
  resources: ["replicasets","daemonsets"]
  verbs: ["watch","list","get"]
- apiGroups: ["policy"]
  resources: ["poddisruptionbudgets"]
  verbs: ["watch","list"]
- apiGroups: ["apps"]
  resources: ["statefulsets"]
  verbs: ["watch","list","get"]
- apiGroups: ["storage.k8s.io"]
  resources: ["storageclasses"]
  verbs: ["get", "list", "watch"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: cluster-autoscaler
  namespace: kube-system
  labels:
    k8s-addon: cluster-autoscaler.addons.k8s.io
    k8s-app: cluster-autoscaler
rules:
- apiGroups: [""]
  resources: ["configmaps"]
  verbs: ["create"]
- apiGroups: [""]
  resources: ["configmaps"]
  resourceNames: ["cluster-autoscaler-status"]
  verbs: ["delete","get","update"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-autoscaler
  labels:
    k8s-addon: cluster-autoscaler.addons.k8s.io
    k8s-app: cluster-autoscaler
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-autoscaler
subjects:
  - kind: ServiceAccount
    name: cluster-autoscaler
    namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cluster-autoscaler
  namespace: kube-system
  labels:
    k8s-addon: cluster-autoscaler.addons.k8s.io
    k8s-app: cluster-autoscaler
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: cluster-autoscaler
subjects:
  - kind: ServiceAccount
    name: cluster-autoscaler
    namespace: kube-system

---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: cluster-autoscaler
  name: cluster-autoscaler
  namespace: kube-system
spec:
  replicas: 1
  selector:
    matchLabels:
      app: cluster-autoscaler
  template:
    metadata:
      labels:
        app: cluster-autoscaler
    spec:
      serviceAccountName: cluster-autoscaler
      containers:
      - image: gcr.io/google-containers/cluster-autoscaler:v1.2.2
        imagePullPolicy: Always
        name: cluster-autoscaler
        resources:
          limits:
            cpu: 100m
            memory: 300Mi
          requests:
            cpu: 100m
            memory: 300Mi
        command:
        - ./cluster-autoscaler
        - --v=3
        - --logtostderr=true
        - --cloud-provider=azure
        - --skip-nodes-with-local-storage=false
        - --nodes=1:$((nodes+4)):$NODE_POOL
        env:
        - name: ARM_SUBSCRIPTION_ID
          valueFrom:
            secretKeyRef:
              key: SubscriptionID
              name: cluster-autoscaler-azure
        - name: ARM_RESOURCE_GROUP
          valueFrom:
            secretKeyRef:
              key: ResourceGroup
              name: cluster-autoscaler-azure
        - name: ARM_TENANT_ID
          valueFrom:
            secretKeyRef:
              key: TenantID
              name: cluster-autoscaler-azure
        - name: ARM_CLIENT_ID
          valueFrom:
            secretKeyRef:
              key: ClientID
              name: cluster-autoscaler-azure
        - name: ARM_CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              key: ClientSecret
              name: cluster-autoscaler-azure
        - name: ARM_VM_TYPE
          valueFrom:
            secretKeyRef:
              key: VMType
              name: cluster-autoscaler-azure
        - name: AZURE_CLUSTER_NAME
          valueFrom:
            secretKeyRef:
              key: ClusterName
              name: cluster-autoscaler-azure
        - name: AZURE_NODE_RESOURCE_GROUP
          valueFrom:
            secretKeyRef:
              key: NodeResourceGroup
              name: cluster-autoscaler-azure
      restartPolicy: Always

EOF
)

if [[ $dryrun != 'yes' ]]; then
        echo "Applying config file..." 
        kubectl apply -f aks-cluster-autoscaler.yml 
	echo "Removing config file..."
	rm aks-cluster-autoscaler.yml > /dev/null 2>&1
else
	echo "Dryrun. In the current directory (`pwd`) file 'aks-cluster-autoscaler.yml' were created. Please, check."
fi

}

accesscluster()
{

#Destroy cluster while destroying or rebuilding nodes
  if [ $cmd == 'destroy' ] || [ $cmd == 'rebuild' ]; then
        echo "Destroying cluster $cluster..."
        case "$provider" in
                GCP)
                        gcloud container clusters delete $cluster --region=$region
                        ;;
                AWS)
                        eksctl delete cluster --name=$cluster --region=$region --wait
                        ;;
                Azure)
                        az aks delete --resource-group ${cluster}_rg --name $cluster	
			az ad app delete --id http://$cluster
			az group delete --name ${cluster}_rg
			rm ~/.azure/aksServicePrincipal.json
	                ;;
        esac
        echo "Cluster destroyed."
	if [ $cmd == 'destroy' ]; then return 0; fi
  fi

#Create cluster while deploying of rebuilding nodes
  if [ $cmd == 'apply' ] || [ $cmd == 'rebuild' ]; then
	echo "Deploying new cluster on $provider..."
	case "$provider" in
        	GCP)
                	gcloud container clusters create $cluster --region=$region --machine-type=n1-standard-2 --num-nodes=$(((nodes+4)/3)) --enable-autoscaling --min-nodes=1 --max-nodes=$(((nodes+4)/3))
                        kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)
                	;;
        	AWS)
			eksctl create cluster --name=$cluster --nodes-min=1 --nodes-max=$((nodes+4)) --region=$region --node-type=m4.large
			;;
		Azure)
			az group create --name ${cluster}_rg --location $region 
			kubversion=`az aks get-versions -l $region --output table | sed -n '3{p;q}' | awk '{print $1}'`
			az aks create --resource-group ${cluster}_rg --name $cluster --node-count $((nodes+4)) --node-vm-size Standard_B2ms --enable-addons monitoring --generate-ssh-keys --kubernetes-version $kubversion
			;;
		*)
                	echo "Parsing error. Provider unknown."
                	exit 3
                	;;
	esac
	echo "Cluster $cluster succesfully deployed on $provider";
  fi

#Get creds for cluster
  echo "Getting credentials for $cluster cluster at $provider..."
  case "$provider" in
  	GCP)
       		gcloud container clusters get-credentials $cluster --region $region
                ;;
        AWS)
                aws eks update-kubeconfig --name $cluster --region $region 
        	;;
	Azure)	
		az aks get-credentials -a --resource-group ${cluster}_rg --name $cluster --overwrite-existing
		;;
        *)
                echo "Parsing error. Provider unknown."
                exit 3
                ;;
  esac
  echo "Received valid credentials."

#Deploy cluster autoscaler
  if [ $cmd == 'apply' ] || [ $cmd == 'rebuild' ]; then
  	case "$provider" in 
        	Azure) 
               		echo "Deploying Azure Autoscaler..." 
                	AzureDeployScaler 
                	echo "Autoscaler deployed." 
                	;; 
  	esac 
  fi
}

applyconfig()
{
  if [ $cmd == 'patch' ] || [ $cmd == 'apply' ] || [ $cmd == 'rebuild']; then
        echo "Applying new config file..."
	kubectl apply -f ./tezos-k8s-config.yml
	echo "Config applied."
  fi 
}

setconfig()
{

echo "Listing $network bootstrap peers..."
case "$network" in
	mainnet)
		p2p=9732
		peers="[\"$(curl -s 'http://api5.tzscan.io/v2/network?state=running&p=0&number=50' | grep -Po '::ffff:([0-9.:]+)' | sed ':a;N;$!ba;s/\n/","/g')\"]"
		;;
	alphanet)	
		p2p=9732
		peers="[\"$(curl -s 'http://api.alphanet.tzscan.io/v2/network?state=running&p=0&number=50' | grep -Po '::ffff:([0-9.:]+)' | sed ':a;N;$!ba;s/\n/","/g')\"]"
		;;
	zeronet)
		p2p=19732
		peers="[\"$(curl -s 'http://api.zeronet.tzscan.io/v2/network?state=running&p=0&number=50' | grep -Po '::ffff:([0-9.:]+)' | sed ':a;N;$!ba;s/\n/","/g')\"]"
		;;

	*)
		echo "Parsing error. Network unknown."
		exit 3
		;;
esac

kubcreds=$(cat ~/.kube/config | base64 -w0)

echo "Generating config file to bring up $nodes Tezos nodes..."

$(cat >| ./tezos-k8s-config.yml <<-EOF
---
apiVersion: v1
kind: Namespace
metadata:
  name: $namespace

---
apiVersion: v1
kind: Secret
metadata:
  name: autoupdater-secret
  annotations:
    kubernetes.io/service-account.name: autoupdater
type: kubernetes.io/service-account-token

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: autoupdater
  namespace: $namespace

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: autoupdater-roles
  namespace: $namespace
rules:
- apiGroups:
  - apps
  - extensions
  resources:
  - statefulsets
  verbs:
  - replace
  - get
  - delete
  - create

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: autoupdater-binding
  namespace: $namespace
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: autoupdater-roles
subjects:
- kind: User
  name: system:serviceaccount:$namespace:autoupdater
  namespace: $namespace
  apiGroup: rbac.authorization.k8s.io

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nfs-data
  namespace: $namespace
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: "standard"
  resources:
    requests:
      storage: ${volume}Gi

---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nfs-server
  namespace: $namespace
spec:
  selector:
    matchLabels:
      role: nfs-server
  template:
    metadata:
      labels:
        role: nfs-server
    spec:
      containers:
      - name: nfs-server
        image: gcr.io/google_containers/volume-nfs:latest
        ports:
          - name: nfs
            containerPort: 2049
          - name: mountd
            containerPort: 20048
          - name: rpcbind
            containerPort: 111
        securityContext:
          privileged: true
        volumeMounts:
          - mountPath: /exports
            name: nfs-data
      volumes:
        - name: nfs-data
          persistentVolumeClaim:
            claimName: nfs-data

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs
  namespace: $namespace
spec:
  capacity:
    storage: ${volume}Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: nfs-server.${namespace}.svc.cluster.local 
    path: "/"

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: shared-storage
  namespace: $namespace
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: ${volume}Gi

---
apiVersion: v1
kind: Service
metadata:
  name: nfs-server
  namespace: $namespace
spec: 
  ports:
    - name: nfs
      port: 2049
    - name: mountd
      port: 20048
    - name: rpcbind
      port: 111
  selector:
    role: nfs-server

---
apiVersion: v1
kind: Service
metadata:
  name: frontend-node
  namespace: $namespace
spec:
  type: NodePort
  selector:
    app: tezos
    component: frontend-node
  ports:
  - protocol: TCP
    port: $p2p
    targetPort: $p2p

---
apiVersion: v1
kind: Service
metadata:
  name: backend-node-management
  namespace: "$namespace"
spec:
  type: ClusterIP
  selector:
    app: tezos
    component: backend-node
  ports:
  - protocol: TCP
    port: $rpc
    targetPort: $rpc

---
apiVersion: v1
kind: Service
metadata:
  name: frontend-node-management
  namespace: "$namespace"
spec:
  type: ClusterIP
  selector:
    app: tezos
    component: frontend-node
  ports:
  - protocol: TCP
    port: $rpc
    targetPort: $rpc

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: baker
  namespace: "$namespace"
  labels:
    app: tezos
    component: baker
spec:
  serviceName: baker-management
  selector:
    matchLabels:
      app: tezos
      component: baker
  template:
    metadata:
      labels:
        app: tezos
        component: baker
    spec:
      initContainers:
      - name: wait-for-node-to-be-online
        image: gophernet/netcat
        command: 
        - sh
        - -c
        - false; while [ \$? -ne 0 ]; do echo "Waiting for node to be online..."; sleep 60; nc -z -w5 backend-node-management.$namespace.svc.cluster.local $rpc; done
      containers:
      - name: baker
        imagePullPolicy: Always
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - ls /usr/local/bin/tezos-baker* | xargs -P0 -I cmd ash -c cmd\\ --port\\ $rpc\\ --addr\\ backend-node-management.$namespace.svc.cluster.local\\ run\\ with\\ local\\ node\\ /var/run/tezos/node/data
        securityContext:
          runAsUser: 100
        volumeMounts:
        - name: node-data
          mountPath: /var/run/tezos/node
          readOnly: true
      volumes:
      - name: node-data
        persistentVolumeClaim:
          claimName: shared-storage

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: endorser
  namespace: "$namespace"
  labels:
    app: tezos
    component: endorser
spec:
  serviceName: endorser-management
  selector:
    matchLabels:
      app: tezos
      component: endorser
  template:
    metadata:
      labels:
        app: tezos
        component: endorser
    spec:
      initContainers:
      - name: wait-for-node-to-be-online
        image: gophernet/netcat
        command: 
        - sh
        - -c
        - false; while [ \$? -ne 0 ]; do echo "Waiting for node to be online..."; nc -z -w5 backend-node-management.$namespace.svc.cluster.local $rpc; done
      containers:
      - name: endorser
        imagePullPolicy: Always 
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - ls /usr/local/bin/tezos-endorser* | xargs -P0 -I cmd ash -c cmd\\ --port\\ $rpc\\ --addr\\ backend-node-management.$namespace.svc.cluster.local\\ run
        securityContext:
          runAsUser: 100

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: accuser
  namespace: "$namespace"
  labels:
    app: tezos
    component: accuser
spec:
  serviceName: accuser-management
  selector:
    matchLabels:
      app: tezos
      component: accuser
  template:
    metadata:
      labels:
        app: tezos
        component: accuser
    spec:
      initContainers:
      - name: wait-for-node-to-be-online
        image: gophernet/netcat
        command: 
        - sh
        - -c
        - false; while [ \$? -ne 0 ]; do echo "Waiting for node to be online..."; nc -z -w5 backend-node-management.$namespace.svc.cluster.local $rpc; done
      containers:
      - name: accuser
        imagePullPolicy: Always
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - ls /usr/local/bin/tezos-accuser* | xargs -P0 -I cmd ash -c cmd\\ --port\\ $rpc\\ --addr\\ backend-node-management.$namespace.svc.cluster.local\\ run 
        securityContext:
          runAsUser: 100

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: backend-node
  namespace: "$namespace"
  labels:
    app: tezos
    component: backend-node
spec:
  serviceName: backend-node-management
  selector:
    matchLabels:
      app: tezos
      component: backend-node
  template:
    metadata:
      labels:
        app: tezos
        component: backend-node
    spec:
      initContainers:
      - name: init-perm 
        image: library/busybox
        command: 
        - sh
        - -c
        - chown -R 100 /mnt/* 
        volumeMounts:
        - name: node-data
          mountPath: /mnt/nd
        securityContext:
          runAsUser: 0
      - name: identity-generate
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - if [ ! -f /var/run/tezos/node/data/identity.json ] ; then  /usr/local/bin/tezos-node identity generate 26 --data-dir=/var/run/tezos/node/data; fi; 
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
      - name: wait-for-node-to-be-online
        image: gophernet/netcat
        command: 
        - sh
        - -c
        - false; while [ \$? -ne 0 ]; do echo "Waiting for node to be online..."; nc -z -w5 frontend-node-management.$namespace.svc.cluster.local $rpc; done
      containers:
      - name: backend-node
        imagePullPolicy: Always
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - /usr/local/bin/tezos-node run --data-dir /var/run/tezos/node/data --rpc-addr 0.0.0.0:$rpc --private-mode --peer frontend-node-management.$namespace.svc.cluster.local:$p2p
        ports:
        - containerPort: $rpc # management
        - containerPort: $p2p # p2p service
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
      securityContext:
          runAsUser: 100
      volumes:
      - name: node-data
        persistentVolumeClaim:
          claimName: shared-storage

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: frontend-nodes
  namespace: "$namespace"
  labels:
    app: tezos
    component: frontend-node
spec:
  serviceName: frontend-node-management
  replicas: $nodes
  selector:
    matchLabels:
      app: tezos
      component: frontend-node
  template:
    metadata:
      labels:
        app: tezos
        component: frontend-node
    spec:
      initContainers:
      - name: init-perm 
        image: library/busybox
        command: 
        - sh
        - -c
        - chown -R 100 /mnt/* 
        volumeMounts:
        - name: node-data
          mountPath: /mnt/nd
        securityContext:
          runAsUser: 0
      - name: identity-generate
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - if [ ! -f /var/run/tezos/node/data/identity.json ] ; then  /usr/local/bin/tezos-node identity generate 26 --data-dir=/var/run/tezos/node/data; fi; 
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
        - name: node-config
          mountPath: /home/tezos/.tezos-node
      - name: bootstrap-node
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - /usr/local/bin/tezos-node run --data-dir /var/run/tezos/node/data --rpc-addr 127.0.0.1:8732 --config-file ~/.tezos-node/config.json & while true; do echo "Waiting until node is bootstrapped"; timeout -t 60 /usr/local/bin/tezos-client bootstrapped; if [ \$? -eq 0 ]; then echo "Node started."; return; else echo "Node still starting. Waiting..."; sleep 60; fi; done 
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
        - name: node-config
          mountPath: /home/tezos/.tezos-node
        securityContext:
          runAsUser: 100
      containers:
      - name: frontend-node
        imagePullPolicy: Always
        image: tezos/tezos:$network
        command:
        - sh
        - -c
        - /usr/local/bin/tezos-node run --data-dir /var/run/tezos/node/data --config-file /home/tezos/.tezos-node/config.json
        ports:
        - containerPort: $rpc # management
        - containerPort: $p2p # p2p service
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
        - name: node-config
          mountPath: /home/tezos/.tezos-node
        securityContext:
          runAsUser: 100
      volumes:
      - name: node-data
        persistentVolumeClaim:
          claimName: node-data
      - name: node-config
        configMap:
          name: configs
          items:
          - key: node-config
            path: config.json
  volumeClaimTemplates:
  - metadata:
      name: node-data
    spec:
      accessModes:
      - ReadWriteOnce
      volumeMode: Filesystem
      resources:
        requests:
          storage: ${volume}Gi

---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: tezos-autoupdater
  namespace: $namespace
  labels:
    app: tezos
    component: autoupdater
spec:
  selector:
    matchLabels:
      app: tezos
      component: autoupdater
  template:
    metadata:
      labels:
        app: tezos
        component: autoupdater
    spec:
      serviceAccountName: autoupdater
      containers:
      - name: autoupdater 
        image: altorosdev/k8s-autoupdater 
        command: 
        - sh
        - -c
        - "/usr/local/bin/entrypoint.sh 86400 tezos/tezos $network statefulset=frontend-nodes tezos/tezos $network statefulset=backend-node tezos/tezos $network statefulset=accuser tezos/tezos $network statefulset=baker tezos/tezos $network statefulset=endorser"
        volumeMounts:
        - name: node-data
          mountPath: "/var/run/tezos/node"
        - name: kubersecretconfig
          mountPath: "/root/.kube/"
          readOnly: true
      volumes:
      - name: node-data
        persistentVolumeClaim:
          claimName: shared-storage
      - name: kubersecretconfig
        secret:
          secretName: kubersecret
          items:
          - key: config
            path: config

---
apiVersion: v1
kind: Secret
metadata:
  name: kubersecret
type: Opaque
data:
  config: $kubcreds
         
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: configs
  namespace: $namespace
data:
  node-config: >
    { "rpc": { "listen-addr": "0.0.0.0:$rpc" },
      "p2p":
      { "bootstrap-peers": ${peers//::ffff:/},  
        "listen-addr": "[::]:$p2p"
      }
    }
EOF
)

echo "Config file generated."

}

echo "This script is designed to deploy and manage a subset of Tezos nodes."
if [ "$#" -eq 0 ] ; then usage ; exit 1; fi


! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "Prerequisites failed. `getopt --test` returned an error in this environment. Getopt is required to get this script work properly."
    exit 1
fi

set -o errexit -o pipefail

OPTIONS=s:c:N:n:p:a:r:V:
LONGOPTS=region:,namespace:,volume:,cluster:,nodes:,network:,provider:,action:,rpc:,dry-run

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi

eval set -- "$PARSED"

namespace="default"
rpc=8732
network="zeronet"
cmd="apply"
volume=100
dryrun="no"
nodes=1

while true; do
    case "$1" in
        -c|--cluster)
            cluster="$2"
            shift 2
            ;;
        -N|--nodes)
            nodes="$2"
            shift 2
            ;;
        -p|--provider)
            provider="$2"
            shift 2
            ;;
        --rpc)
            rpc="$2"
            shift 2
            ;;
        -n|--network)
            network="$2"
            shift 2
            ;;
	-r|--region)
	    region="$2"
	    shift 2
	    ;;
        -a|--action)
            cmd="$2"
            shift 2
            ;;
        -V|--volume)
            cdvolume="$2"
            shift 2
            ;;
        -s|--namespace)
            namespace="$2"
            shift 2
            ;;
	--dry-run)
	    dryrun='yes';
	    shift 1
	    ;;
        --)
            shift
            break
            ;;
        *)
            echo "Parsing error. Option unknown: $1"
            exit 3
            ;;
    esac
done

if [[ -z "${region+x}" || -z "${cluster+x}" || -z "${provider+x}" ]]; then
    usage
    echo "$0: Not all required options were set. Make sure that provider, region and cluster name are specified."
    exit 4
fi

case "$provider" in
	GCP)
		GoogleAuth
		;;
	AWS)
		AWSAuth
		;;
	Azure)
		AzureAuth
		;;
	*)
		echo "Parsing error. Provider unknown."
		exit 3
		;;
esac

if [[ $dryrun != 'yes' ]]; then
  accesscluster       
else    
  echo "Dryrun. Cluster were not created."
fi

if [[ $cmd != 'destroy' ]]; then	
  setconfig 
fi

if [[ $dryrun != 'yes' ]]; then
  applyconfig
  rm ./tezos-k8s-config.yml > /dev/null 2>&1 
  echo "Cluster were created. Please, check status of your cluster at $provider cloud console. Please, note that it might take up to several hours to sync full blockchain and bring node online. Thank you for your patience."
else
  echo "Dryrun. In the current directory (`pwd`) file 'tezos-k8s-config.yml' were created. Please, check."
fi
